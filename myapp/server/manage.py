'''
Manage the application
'''

from flask.ext.script import Manager
from flask.ext.migrate import Migrate, MigrateCommand
from flask import session
from myapp import app, db
from myapp.models import User, Post

migrate = Migrate(app, db)
manager = Manager(app)

# migration
manager.add_command('db', MigrateCommand)

@manager.command
def create_db():
  # Creates the DB tables
  db.create_all()

@manager.command
def drop_db():
  # Drop all DB tables
  db.drop_all()

@manager.command
def create_admin():
  # Create the admin user
  admin = User(username='admin', email='ad@min.com', password='admin password', admin=True)
  db.session.add(admin)
  db.session.commit()

@manager.command
def create_data():
  # Create sample data
  session['user'] = User.query.filter_by(username='admin').first()
  post = Post('Title', 'Body')
  db.session.add(post)
  db.session.commit()
  session.pop('user',None)

if __name__ == '__main__':
  manager.run()