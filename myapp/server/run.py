'''
Run the server
'''

if __name__ == '__main__':
  from myapp import app
  app.run(debug=True)