'''
APP
'''

from require import *

# Base Directory
basedir = os.path.join(os.path.abspath(os.path.dirname(__file__)), '..')


# Create App
app = Flask(__name__)
app.config.from_object('myapp.config')

# Database
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'app.sqlite')
db = SQLAlchemy(app)

# Bcrypt for password encryption
flask_bcrypt = Bcrypt(app)

# Import Models
from models import *

# Create DB
db.create_all()

# HTTPAuth
auth = HTTPBasicAuth()

# Post Request Response
@app.after_request
def after_resquest(response):
  response.headers.add('Access-Control-Allow-Origin', '*')
  response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
  response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
  return response

# Finally the Views
import controller