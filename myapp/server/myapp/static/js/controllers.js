// MyApp Controllers
'use strict';

var myAppControllers = angular.module('myAppControllers', []);

myAppControllers.controller('IndexController', [
  '$scope', function($scope) {

  }
]);

myAppControllers.controller('AboutController', [
  '$scope', function($scope) {

  }
]);

myAppControllers.controller('LoginController', [
  '$scope', '$location', 'AuthService', function($scope, $location, AuthService) {
    $scope.login = function() {
      // init values
      $scope.error = false;
      $scope.disabled = true;

      // call login from service
      AuthService.login($scope.loginForm.username, $scope.loginForm.password)
      // handle success
      .then(function() {
        $location.path('/');
        $scope.disabled = false;
        $scope.loginForm = {};
      })
      // handle error
      .catch(function() {
        $scope.error = true;
        $scope.errorMessage = "Invalid username and/or password";
        $scope.disabled = false;
        $scope.loginForm = {};
      });
    };
  }
]);

myAppControllers.controller('LogoutController', [
  '$scope', '$location', 'AuthService', function($scope, $location, AuthService) {
    $scope.logout = function() {
      AuthService.logout()
      .then(function() {
        $location.path('/login');
      })
    };
  }
]);

myAppControllers.controller('RegisterController', [
  '$scope', '$location', 'AuthService', function($scope, $location, AuthService) {
    $scope.register = function() {
      // init
      $scope.error = false;
      $scope.disabled = true;

      // Call the login service
      AuthService.register($scope.registerForm.username, $scope.registerForm.email, $scope.registerForm.password)
      // Success
      .then(function() {
        $location.path('/');
        $scope.disabled = false;
        $scope.registerForm = {};
      })
      // Failure
      .catch(function() {
        $scope.error = true;
        $scope.errorMessage = "Could not complete registration.";
        $scope.disabled = false;
        $scope.registerForm = {};
      });
    };
  }
]);

myAppControllers.controller('PostListController', [
  '$scope', 'Post', function($scope, Post) {
    var postsQuery = Post.get({}, function(posts) {
      $scope.posts = posts.objects;
    });
  }
]);

myAppControllers.controller('PostDetailController', [
  '$scope', '$routeParams', 'Post', function($scope, $routeParams, Post) {
    var postQuery = Post.get({postId: $routeParams.postId}, function(post) {
      console.log(post);
      $scope.post = post;
    });
  }
]);

myAppControllers.controller('PostCreateController', [
  '$scope', '$routeParams', '$location', 'Post', function($scope, $routeParams, $location, Post) {
    $scope.create = function() {
      // init
      $scope.error = false;
      $scope.disabled = true;

      var post = new Post({title: $scope.postCreateForm.title, body: $scope.postCreateForm.body})
      post.$save(function(p, repsonseHeaders) {
        $location.path("/post/"+p.id);
      });
    }
  }
]);