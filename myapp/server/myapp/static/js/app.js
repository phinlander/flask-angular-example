// MyApp App.js
'use strict';

// Include the router
var myApp = angular.module('myApp', ['myServices','ngRoute', 'myAppControllers']);

myApp.config(['$routeProvider', '$locationProvider',
  function($routeProvider, $locationProvider) {
  $routeProvider
  .when('/', {
    templateUrl: 'static/partials/index.html',
    resolve: resolver({req_login: true}),
    controller: 'IndexController'
  })
  .when('/about', {
    templateUrl: 'static/partials/about.html',
    resolve: resolver({req_login: false}),
    controller: 'AboutController'
  })
  .when("/asdf", {
    templateUrl: 'static/partials/asdf.html',
    resolve: resolver({req_login: true}),
    controller: 'AboutController'
  })
  .when('/login', {
    templateUrl: 'static/partials/login.html',
    resolve: resolver({req_login: false}),
    controller: 'LoginController'
  })
  .when('/logout', {
    templateUrl: 'static/partials/logout.html',
    resolve: resolver({req_login: true}),
    controller: 'LogoutController'
  })
  .when('/register', {
    templateUrl: 'static/partials/register.html',
    resolve: resolver({req_login: false}),
    controller: 'RegisterController'
  })
  .when('/post', {
    templateUrl: 'static/partials/post-list.html',
    resolve: resolver({req_login: false}),
    controller: 'PostListController'
  })
  .when('/post/create', {
    templateUrl: 'static/partials/post-create.html',
    resolve: resolver({req_admin: true}),
    controller: 'PostCreateController'
  })
  .when('/post/:postId', {
    templateUrl: 'static/partials/post-detail.html',
    resolve: resolver({req_login: true}),
    controller: 'PostDetailController'
  })
  .otherwise({
    redirectTo: '/'
  });

  $locationProvider.html5Mode(true);
}]);

var resolver = function(access) {
  return {
    load: function($q, AuthService, $location) {
      var deferred = $q.defer();
      if(access.req_admin && AuthService.isAdmin() === false) {
        deferred.reject();
        $location.url("/");
      } else if(access.req_login && AuthService.isLoggedIn() === false) {
        deferred.reject();
        $location.url("/login");
      } else {
        deferred.resolve();
      }
      return deferred.promise;
    }
  }
}

myApp.run(function($rootScope, $location, $route, AuthService) {
});