'use strict';

angular.module('myServices', ['ngResource'])
.factory('Post', function($resource) {
  return $resource('/api/post/:postId', {}, {
    
  })
})
.factory('AuthService', ['$q', '$timeout', '$http',
  function($q, $timeout, $http) {
    // create user
    var user = false;
    var admin = false;
    // return available functions for use in controllers
    return ({
      isLoggedIn: isLoggedIn,
      isLoggedInPost: isLoggedInPost,
      isAdmin: isAdmin,
      isAdminPost: isAdminPost,
      login: login,
      logout: logout,
      register: register
    });

    function isLoggedInPost() {
      var deferred = $q.defer();
      $http.get('/api/isloggedin')
      .success(function(data, status) {
        if(status === 200 && data.user) {
          user = data.user;
          deferred.resolve();
        } else {
          user = false;
          deferred.reject();
        }
      });
      return deferred.promise;
    }

    function isLoggedIn() {
      if(user) {
        return true;
      } else {
        user = false;
        isLoggedInPost()
        .then(function() {
          user = true;
          return user;
        });
      }
    };

    function isAdminPost() {
      var deferred = $q.defer();
      $http.get('/api/isadmin')
      .success(function(data, status) {
        if(status === 200 && data.admin) {
          admin = data.admin;
          deferred.resolve();
        } else {
          admin = false;
          deferred.reject();
        }
      });
      return deferred.promise;
    }

    function isAdmin() {
      if(isLoggedIn()) {
        if(admin) {
          return true;
        }
        isAdminPost()
        .then(function() {
          return true
        })
        .catch(function() {
          return false;
        });
      } else {
        return false;
      }
    }

    function login(username, password) {
      // create a new instance of deferred
      var deferred = $q.defer();

      // send a post request to the server
      $http.post('/api/login', {username: username, password: password})
      // handle success 
      .success(function(data, status) {
        if(status === 200 && data.user) {
          user = data.user;
          admin = data.admin;
          deferred.resolve();
        } else {
          user = false;
          admin = false;
          deferred.reject();
        }
      });

      // return promise object
      return deferred.promise;
    };

    function logout() {
      // create a new instance of deferred
      var deferred = $q.defer();

      // send a get request to the server
      $http.get('/api/logout')
      // handle success
      .success(function(data) {
        user = false;
        admin = false;
        deferred.resolve();
      })
      // handle error
      .error(function(data) {
        deferred.reject();
      });

      // return promise object
      return deferred.promise;
    };

    function register(username, email, password) {
      // create a new instance of deferred
      var deferred = $q.defer();

      // send a post request to the server
      $http.post('/api/register', {username:username, email:email, password:password})
      .success(function(data, status) {
        if(status === 200 && data.result) {
          deferred.resolve();
        } else {
          deferred.reject();
        }
      })
      .error(function(data) {
        deferred.reject();
      });

      return deferred.promise;
    };
  }
]);