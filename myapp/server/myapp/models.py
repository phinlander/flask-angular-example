'''
APP MODELS
'''

from require import *

from myapp import app, db, flask_bcrypt

class User(db.Model):
  id = db.Column(
    db.Integer,
    primary_key=True,
    autoincrement=True
  )
  username = db.Column(
    db.String(120),
    unique=True,
    nullable=False,
  )
  email = db.Column(
    db.String(120),
    unique=True,
    nullable=False
  )
  password = db.Column(
    db.String(80),
    nullable=False
  )
  admin = db.Column(
    db.Boolean,
    default=False
  )
  posts = db.relationship(
    'Post',
    backref='user',
    lazy='dynamic'
  )

  def __init__(self, username, email, password, admin=False):
    self.username = username
    self.email = email
    self.password = flask_bcrypt.generate_password_hash(password)
    self.admin = admin

  def is_authenticated(self):
    return True

  def is_admin(self):
    return self.admin

  def is_active(self):
    return True

  def is_anonymous(self):
    return False

  def get_id(self):
    return unicode(self.id)

  def __repr__(self):
    return '<User %r>' % self.email

class Post(db.Model):
  id = db.Column(
    db.Integer,
    primary_key=True
  )
  title = db.Column(
      db.String(120),
      nullable=False
  )
  body = db.Column(
    db.Text,
    nullable=False
  )
  user_id = db.Column(
    db.Integer,
    db.ForeignKey('user.id')
  )
  createdAt = db.Column(
    db.DateTime,
    default=db.func.now()
  )

  def username(self):
    return self.user.username

  def __repr__(self):
    return '<Post %r>' % self.title

  def __init__(self, title, body):
    self.title = title
    self.body = body
    self.user_id = session.get('userid')
