'''
MYAPP CONTROLLER
'''

from require import *

from myapp import app, db, flask_bcrypt, auth

from myapp.models import *

###############
# Basic Views #
###############

# routing for basic pages
@app.route('/')
@app.route('/<model_name>/')
@app.route('/<model_name>/create')
@app.route('/<model_name>/<item_id>')
@app.route('/<model_name>/<item_id>/edit')
def basic_pages(**kwargs):
  return app.send_static_file('base.html')

# special file handlers and error handlers
@app.route('/favicon.ico')
def favicon():
  return send_from_directory(os.path.join(app.basedir, 'static'), 'img/favicon.ico')

@app.errorhandler(404)
def page_not_found(e):
  return render_template('404.html'), 404

###############
# Basic Views #
###############



#########
#  API  #
#########

###
# API Setup

# Pre and post process
def logged_in(*args, **kw):
  if not session.get('logged_in'):
    raise ProcessingException(description='Not authenticated!', code=401)

def is_admin(*args, **kw):
  if not session.get('is_admin'):
    raise ProcessingException(description='Not authenticated!', code=401)

# Create manager
api_manager = APIManager(
  app,
  flask_sqlalchemy_db=db,
  preprocessors={
    'POST':[logged_in]
  }
)

api_manager.create_api(
  User,
  methods=['GET','POST','PATCH','DELETE'],
  allow_patch_many=True,
  preprocessors={
    'GET_SINGLE':[logged_in],
    'GET_MANY':[logged_in],
    'POST':[is_admin],
    'PATCH_SINGLE':[is_admin],
    'PATCH_MANY':[is_admin],
    'DELETE_SINGLE':[is_admin],
    'DELETE_MANY':[is_admin]
  }
)

api_manager.create_api(
  Post,
  methods=['GET','POST','PATCH','DELETE'],
  allow_patch_many=True,
  include_columns = ['id','title','body','createdAt'],
  include_methods = ['username'],
  preprocessors={
    'POST':[logged_in],
    'PATCH_SINGLE':[is_admin],
    'PATCH_MANY':[is_admin],
    'DELETE_SINGLE':[is_admin],
    'DELETE_MANY':[is_admin]
  }
)

dbsession = api_manager.session

# API Setup
###


###
# Register


###
# Register/Login/Logout/Session

@app.route('/api/register', methods=['POST'])
def register():
  json_data = request.json
  user = User(
    username=json_data['username'],
    email=json_data['email'],
    password=json_data['password']
  )
  print(user)
  try:
    db.session.add(user)
    db.session.commit()
    status = 'success'
  except:
    status = 'this user is already registered'
  db.session.close()
  session['user'] = user
  session['logged_in'] = True
  session['is_admin'] = user.is_admin()
  return jsonify(result=status)

@app.route('/api/login', methods=['POST'])
def login():
  json_data = request.json
  user = User.query.filter_by(username=json_data['username']).first()
  if user and flask_bcrypt.check_password_hash(user.password,json_data['password']):
    session['userid'] = user.id
    session['logged_in'] = True
    session['is_admin'] = user.is_admin()
  isuser = True if session.get('logged_in') else False
  isadmin = True if session.get('is_admin') else False
  return jsonify(user=isuser, admin=isadmin)

@app.route('/api/logout', methods=['GET'])
def logout():
  session.pop('user',None)
  session.pop('logged_in',None)
  session.pop('is_admin',None)
  return jsonify(result='success')

@app.route('/api/isloggedin', methods=['GET'])
def isloggedin():
  return jsonify(user=True if session.get('logged_in') else False)

@app.route('/api/isadmin', methods=['GET'])
def isadmin():
  return jsonify(admin=True if session.get('is_admin') else False)


# Login/Logout
###

#########
#  API  #
#########